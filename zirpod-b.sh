#!/bin/sh
#==================================
# Welcome to ZachIR's PolyDistro Bootstrapping Script
# This script is designed to bootstrap one of the available linux
# distributions in a minimal way. In the spirit of maximum modularity,
# ZIRPoD has been written in pure "sh", and tested with POSIX-
# Compliant dash.
# For the moment, it is necessary to run this script in 
#==================================

# Place below the codename for the distro you wish to install, as listed below
#
# Artix (Artix Linux, rolling)
# Arch (Arch Linux, rolling)
# Buster (Debian GNU/Linux, stable)
# Bullseye (Debian GNU/Linux, testing)
# Sid (Debian GNU/Linux, unstable)
# ASCII (Devuan GNU+Linux, oldstable)
# Beowulf (Devuan GNU+Linux, stable)
# Chimaera (Devuan GNU+Linux, testing)
# Ceres (Devuan GNU+Linux, unstable)
# Xenial (Ubuntu Linux, 16.04 LTS)
# Bionic (Ubuntu Linux, 18.04 LTS)
# Focal (Ubuntu Linux, 20.04 LTS)
# Groovy (Ubuntu Linux, 20.10)
DISTRO=

# Place the codename for the distro you are installing from
#
# Artix (Artix Linux, rolling)
# Arch (Arch Linux, rolling)
# Manjaro (Manjaro Linux, rolling)
IDISTRO=
LSB_RELEASE="$(cat /etc/lsb-release | cut -d'=' -f2)"

# The below line is included so it will be easy to update the script with newer versions of artools-base
ARTOOLS_PKG_NAME="artools-base-0.22.3-2-any.pkg.tar.zst"

# The below 3 lines are included as the repos debootstrap will attempt to install from. If debootstrap fails to connect,
# changing these to a different mirror should fix it
DEBIAN_DEBOOTSTRAP_MIRROR="http://deb.debian.org/debian"
DEVUAN_DEBOOTSTRAP_MIRROR="http://mirror.math.princeton.edu/pub/devuan"
UBUNTU_DEBOOTSTRAP_MIRROR="http://archive.ubuntu.com/ubuntu"

# functions {{{
#==================================
# This section defines functions to be used in the other sections

error() {
  echo "$1"
  exit -1;
}

archbased () {
  if [ "$IDISTRO" = "Arch" -o "$IDISTRO" = "Artix" -o "$IDISTRO" = "Manjaro" ]; then
    echo "true"
  fi
}

distrois () {
  if [ "$DISTRO" = "Buster" -o "$DISTRO" = "Bullseye" -o "$DISTRO" = "Sid" ]; then
    echo "Debian"
  elif [ "$DISTRO" = "ASCII" -o "$DISTRO" = "Beowulf" -o "$DISTRO" = "Chimaera" -o "$DISTRO" = "Ceres" ]; then
    echo "Devuan"
  elif [ "$DISTRO" = "Xenial" -o "$DISTRO" = "Bionic" -o "$DISTRO" = "Focal" -o "$DISTRO" = "Groovy" ]; then
    echo "Ubuntu"
  elif [ "$DISTRO" = "Arch" ]; then
    echo "Arch"
  elif [ "$DISTRO" = "Artix" ]; then
    echo "Artix"
  else
    error "Distro $DISTRO is not supported."
  fi
}

# End functions
#==================================
#}}}

# pre-bootstrap {{{
#==================================
# This section of script is to acquire and install the correct bootstrap utility

BOOTSTRAP_URL=
BOOTSTRAP_PKG=
if [ "$DISTRO" = "Artix" -a "$IDISTRO" = "Arch" ]; then
  PKG_URL="https://artix.wheaton.edu/repos/world/os/x86_64/"
  PKG_NAME="$ARTOOLS_PKG_NAME"
  BOOTSTRAP_URL="$PKG_URL$PKG_NAME"
  BOOTSTRAP_DEPENDENCIES="awk bash coreutils pacman util-linux asciidoc git make glibc"
elif [ -n "$(archbased)" ]; then
  case "$DISTRO" in
    "Artix") BOOTSTRAP_PKG="artools-base"
      BOOTSTRAP_DEPENDENCIES="awk bash coreutils pacman util-linux asciidoc git make glibc"
      ;;
    "Arch") BOOTSTRAP_PKG="arch-install-scripts"
      BOOTSTRAP_DEPENDENCIES="awk bash coreutils pacman util-linux asciidoc git make glibc" 
      ;;
    "Buster"|"Bullseye"|"Sid"|"ASCII"|"Beowulf"|"Chimaera"|"Ceres"|"Xenial"|"Bionic"|"Focal"|"Groovy")
      BOOTSTRAP_PKG="debootstrap"
      BOOTSTRAP_DEPENDENCIES="binutils perl wget debian-archive-keyring gnupg ubuntu-keyring"
      ;;
    *)
      error "Any other releases are not presently supported. They might be in the future, though!"
  esac
fi

if [ -n "$(archbased)" ]; then
  sudo pacman -Sy $BOOTSTRAP_DEPENDENCIES
  if [ -n "$BOOTSTRAP_URL" ]; then
    curl -o $PKG_NAME $BOOTSTRAP_URL
    sudo pacman -U $PKG_NAME
  else
    sudo pacman -S $BOOTSTRAP_PKG
  fi
else
  error "No other distros are presently supported for bootstrapping from. They might be in the future, though!"
fi

# End acquiring bootstrapper
#==================================
#}}}

# bootstrap {{{
#==================================
# This section is to bootstrap a (very) minimal install of the OS
# No extraneous packages will be installed in this step
# This script currently assumes filesystems are mounted in proper heirarchy starting at /mnt

if [ "$DISTRO" = "Arch" ]; then
  which pacstrap || error "Pacstrap was not found! Something must have gone wrong in pre-bootstrap stage"
  pacstrap -C arch/arch-pacman.conf /mnt base linux linux-firmware
  echo "Minimal system installed. You don't want it to end here though."
elif [ "$DISTRO" = "Artix" ]; then
  which basestrap || error "Basestrap was not found! Something must have gone wrong in pre-bootstrap stage"
  basestrap -M -G -C arch/artix-pacman.conf /mnt base runit elogind-runit linux linux-firmware
  echo "Minimal system installed. You don't want it to end here though."
elif [ "$(distrois)" = "Debian" -o "$(distrois)" = "Devuan" -o "$(distrois)" = "Ubuntu" ]; then
  which debootstrap || error "Debootstrap was not found! Something must have gone wrong in pre-bootstrap stage"
  case "$DISTRO" in
    "Buster") debootstrap buster /mnt "$DEBIAN_DEBOOTSTRAP_MIRROR" ;;
    "Bullseye") debootstrap bullseye /mnt "$DEBIAN_DEBOOTSTRAP_MIRROR" ;;
    "Sid") debootstrap sid /mnt "$DEBIAN_DEBOOTSTRAP_MIRROR" ;;
    "ASCII") debootstrap ascii /mnt "$DEVUAN_DEBOOTSTRAP_MIRROR" ;;
    "Beowulf") debootstrap beowulf /mnt "$DEVUAN_DEBOOTSTRAP_MIRROR" ;;
    "Chimaera") debootstrap chimaera /mnt "$DEVUAN_DEBOOTSTRAP_MIRROR" ;;
    "Ceres") debootstrap ceres /mnt "$DEVUAN_DEBOOTSTRAP_MIRROR" ;;
    "Xenial") debootstrap xenial /mnt "$UBUNTU_DEBOOTSTRAP_MIRROR" ;;
    "Bionic") debootstrap bionic /mnt "$UBUNTU_DEBOOTSTRAP_MIRROR" ;;
    "Focal") debootstrap focal /mnt "$UBUNTU_DEBOOTSTRAP_MIRROR" ;;
    "Groovy") debootstrap groovy /mnt "$UBUNTU_DEBOOTSTRAP_MIRROR" ;;
  esac
  echo "Minimal system installed. You don't want it to end here though."
fi

# End bootstrap
#==================================
#}}}
